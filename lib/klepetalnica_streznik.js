var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliGesla = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebno(socket);
    obdelajPridruzitevKanaluGeslo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      socket.emit('uporabniki', obdelavaUporabnikov(socket));
    })
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  var clients = io.sockets.clients();
  var zacasnaGesla = {};
  for(var i = 0; i < clients.length; i++) {
    if((trenutniKanal[clients[i].id] in kanaliGesla)) {
      zacasnaGesla[trenutniKanal[clients[i].id]] = kanaliGesla[trenutniKanal[clients[i].id]];
    }
  }
  kanaliGesla = zacasnaGesla;
}

function pridruzitevKanaluGeslo(socket, kanal) {
  socket.join(kanal.novKanal);
  trenutniKanal[socket.id] = kanal.novKanal;
  kanaliGesla[kanal.novKanal] = kanal.geslo;
  socket.emit('pridruzitevOdgovor', {kanal: kanal.novKanal});
  socket.broadcast.to(kanal.novKanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal.novKanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal.novKanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal.novKanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  //console.log(kanaliGesla);
  var clients = io.sockets.clients();
  //console.log(clients);
  var zacasnaGesla = {};
  for(var i = 0; i < clients.length; i++) {
    if((trenutniKanal[clients[i].id] in kanaliGesla)) {
      zacasnaGesla[trenutniKanal[clients[i].id]] = kanaliGesla[trenutniKanal[clients[i].id]];
    }
    //console.log(trenutniKanal[clients[i].id]);
    //console.log(kanaliGesla[trenutniKanal[clients[i]]]);
  }
  kanaliGesla = zacasnaGesla;
  //console.log(kanaliGesla);
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelavaUporabnikov(socket) {
  var trenutniUporabniki = [];
  var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[socket.id]);
  
  for(var i=0; i < uporabnikiNaKanalu.length; i++) {
    trenutniUporabniki.push(vzdevkiGledeNaSocket[uporabnikiNaKanalu[i].id]);
  }
  
  return trenutniUporabniki;
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if (kanal.novKanal in kanaliGesla) {
      socket.emit('pridruzitevKanalNapacno', kanal);
      return;
    } else {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal);
    }
  });
}

function obdelajPridruzitevKanaluGeslo(socket) {
  socket.on('pridruzitevZahtevaGeslo', function(kanal) {
    var clients = io.sockets.clients();
    //console.log(clients);
    //console.log(trenutniKanal);
    //console.log(trenutniKanal[clients[0].id]);
    for(var i = 0; i < clients.length; i++) {
      if(trenutniKanal[clients[i].id] === kanal.novKanal) {
        if (kanal.novKanal in kanaliGesla) {
          if (kanaliGesla[kanal.novKanal] === kanal.geslo) {
            socket.leave(trenutniKanal[socket.id]);
            pridruzitevKanalu(socket, kanal.novKanal);
            return;
          } else  {
            socket.emit('pridruzitevKanalNapacno', kanal);
            return;
          }
        } else {
          socket.emit('pridruzitevKanalObstaja', kanal);
          return;
        }
      }
    }
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanaluGeslo(socket, kanal);
    //console.log(kanal);
  });
}

function obdelajZasebno(socket) {
  socket.on('zasebnoZahteva', function(zasebno) {
    if (uporabljeniVzdevki.indexOf(zasebno.vzdevek) == -1) {
      socket.emit('zasebnoNapacenVzdevek', zasebno);
      return;
    } else if (zasebno.vzdevek === vzdevkiGledeNaSocket[socket.id]) {
      socket.emit('zasebnoNapacenVzdevek', zasebno);
      return;
    } else {
      socket.emit('zasebnoPosiljatelj', zasebno);
      var clients = io.sockets.clients();
      var posiljatelj = [vzdevkiGledeNaSocket[socket.id], zasebno.sporocilo];
      for (var i = 0; i < clients.length; i++) {
        if(zasebno.vzdevek === vzdevkiGledeNaSocket[clients[i].id]) {
          clients[i].emit('zasebnoPrejemnik', posiljatelj);
        }
      }
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}