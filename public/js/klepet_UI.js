var smileys = {};
smileys[0] = { smiley:";)", link:"<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>" };
smileys[1] = { smiley:":)", link: "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>"};
smileys[2] = { smiley:"(y)", link: "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>" };
smileys[3] = { smiley:":*", link: "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>" };
smileys[4] = { smiley:":(", link: "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>" };
var swearWords = [];

$.get("swearWords.xml", function (xml){
  
  //console.log(xml);
  
  var allWords = $(xml).find("word");
  
  //console.log(allWords);
  
  allWords.each (function (index, item){
    swearWords.push($(item).text());
  });

});

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


function divElementSmiley(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}


var vzdevek = "";
var kanal = "";


function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  //console.log(swearWords);
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    if(sporocilo.search("<script>") !== -1) {
      klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      $('#poslji-sporocilo').val('');
      return;
    } else {
      var checkAgain = true;
      
      function smileyCheck(index) {
        if(sporocilo.indexOf(smileys[index]["smiley"]) > -1){
          sporocilo = sporocilo.replace(smileys[index]["smiley"], smileys[index]["link"]);
          checkAgain = true;
        }
      }
      
      while(checkAgain === true) {
        checkAgain = false;
        smileyCheck(0)
        smileyCheck(1)
        smileyCheck(2)
        smileyCheck(3)
        smileyCheck(4)
      }
    }
    var istaBeseda = false;
    for (var i = 0; i < swearWords.length; i++) {
      istaBeseda = false;
      var testBeseda = new RegExp("\\b"+swearWords[i]+"\\b", "i");
      //console.log(testBeseda);
      if(sporocilo.search(testBeseda) > -1) {
        //console.log(testBeseda);
        istaBeseda = true;
        //console.log(swearWords[i]);
        var censored = "";
        for(var j=0; j < swearWords[i].length; j++) {
          censored += "*";
        }
        sporocilo = sporocilo.replace(testBeseda, censored);
      }
      
      if (istaBeseda === true) {
        i--;
      }
      
    }
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementSmiley(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      vzdevek = rezultat.vzdevek;
      $('#kanal').text(vzdevek + " @ " + kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevKanalObstaja', function(kanal) {
    var odgovor = "Izbrani kanal "+ kanal.novKanal +" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+ kanal.novKanal +" ali zahtevajte kreiranje kanala z drugim imenom.";
    $('#sporocila').append(divElementHtmlTekst(odgovor));
  });

  socket.on('pridruzitevKanalNapacno', function(kanal) {
    var odgovor =  "Pridružitev v kanal "+ kanal.novKanal +" ni bilo uspešno, ker je geslo napačno!";
    $('#sporocila').append(divElementHtmlTekst(odgovor));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    kanal = rezultat.kanal;
    $('#kanal').text(vzdevek + " @ " + kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    if(sporocilo.besedilo.search("<script>") === -1){
      var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    } else {
      var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    }
    $('#sporocila').append(novElement);
  });
  
  socket.on('zasebnoNapacenVzdevek', function(zasebno) {
    var odgovor = 'Sporočila "'+ zasebno.sporocilo +'" uporabniku z vzdevkom "'+zasebno.vzdevek  +'" ni bilo mogoče posredovati.';
    $('#sporocila').append(divElementHtmlTekst(odgovor));
  });
  
  socket.on('zasebnoPosiljatelj', function(zasebno) {
    var odgovor = '(zasebno za '+ zasebno.vzdevek +'): '+ zasebno.sporocilo;
    var checkAgain = true;
    
    if(odgovor.search("<script>") !== -1) {
      $('#sporocila').append(divElementEnostavniTekst(odgovor));
    }
      
    function smileyCheck(index) {
      if(odgovor.indexOf(smileys[index]["smiley"]) > -1){
        odgovor = odgovor.replace(smileys[index]["smiley"], smileys[index]["link"]);
        checkAgain = true;
      }
    }
      
    while(checkAgain === true) {
      checkAgain = false;
      smileyCheck(0)
      smileyCheck(1)
      smileyCheck(2)
      smileyCheck(3)
      smileyCheck(4)
    }
    
    var istaBeseda = false;
    for (var i = 0; i < swearWords.length; i++) {
      istaBeseda = false;
      var testBeseda = new RegExp("\\b"+swearWords[i]+"\\b", "i");
      //console.log(testBeseda);
      if(odgovor.search(testBeseda) > -1) {
        //console.log(testBeseda);
        istaBeseda = true;
        //console.log(swearWords[i]);
        var censored = "";
        for(var j=0; j < swearWords[i].length; j++) {
          censored += "*";
        }
        odgovor = odgovor.replace(testBeseda, censored);
      }
      
      if (istaBeseda === true) {
        i--;
      }
      
    }
    $('#sporocila').append(divElementSmiley(odgovor));
  });
  
  socket.on('zasebnoPrejemnik', function(posiljatelj) {
    var odgovor = posiljatelj[0] + " (zasebno): " + posiljatelj[1];
    var checkAgain = true;
    
    if(odgovor.search("<script>") !== -1) {
      $('#sporocila').append(divElementEnostavniTekst(odgovor));
    }
      
    function smileyCheck(index) {
      if(odgovor.indexOf(smileys[index]["smiley"]) > -1){
        odgovor = odgovor.replace(smileys[index]["smiley"], smileys[index]["link"]);
        checkAgain = true;
      }
    }
      
    while(checkAgain === true) {
      checkAgain = false;
      smileyCheck(0)
      smileyCheck(1)
      smileyCheck(2)
      smileyCheck(3)
      smileyCheck(4)
    }
    
    var istaBeseda = false;
    for (var i = 0; i < swearWords.length; i++) {
      istaBeseda = false;
      var testBeseda = new RegExp("\\b"+swearWords[i]+"\\b", "i");
      //console.log(testBeseda);
      if(odgovor.search(testBeseda) > -1) {
        //console.log(testBeseda);
        istaBeseda = true;
        //console.log(swearWords[i]);
        var censored = "";
        for(var j=0; j < swearWords[i].length; j++) {
          censored += "*";
        }
        odgovor = odgovor.replace(testBeseda, censored);
      }
      
      if (istaBeseda === true) {
        i--;
      }
      
    }
    $('#sporocila').append(divElementSmiley(odgovor));
  })

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
      $('#seznam-uporabnikov').empty();
      
      for (var i = 0; i < uporabniki.length; i++) {
        if(uporabniki[i] !== "") {
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
        }
      }
  })

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});