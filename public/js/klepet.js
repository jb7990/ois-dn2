var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.posljiZasebno = function(zasebno) {
  this.socket.emit('zasebnoZahteva', {
    vzdevek: zasebno[0],
    sporocilo: zasebno[1]
  });
  //console.log(zasebno);
}; 

Klepet.prototype.spremeniKanalGeslo = function(kanal) {
  this.socket.emit('pridruzitevZahtevaGeslo', {
    novKanal: kanal[0],
    geslo: kanal[1]
  });
}


Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = "";
      //console.log(besede);
      if (besede.length === 2 && (besede.join(' ').match(/"/g) || []).length == 4) {
        var polnoSporocilo = besede.join(' ');
        while(polnoSporocilo.search(/"/g) > -1){
          polnoSporocilo = polnoSporocilo.replace(/["]+/g, '');
        }
        kanal = polnoSporocilo.split(" ");
        console.log(kanal[0].length);
        if(kanal[0].length === 0 || kanal[1].length === 0) {
          break;
        }
        this.spremeniKanalGeslo(kanal);
        //console.log(kanal);
        //console.log(geslo);
      } else {
        kanal = besede.join(' ');
        //console.log(kanal);
        this.spremeniKanal(kanal);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      for (var i = 0; i < besede.length; i++) {
        while(besede[i].search(/"/g) > -1){
          besede[i] = besede[i].replace(/["]+/g, '');
        }
      }
      //console.log(besede);
      this.posljiZasebno(besede);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};